﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Crud2323.Startup))]
namespace Crud2323
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
