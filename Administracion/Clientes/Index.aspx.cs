﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Administracion_Clientes_Index : System.Web.UI.Page
{ //poner een el foco para importar librerias 
    readonly SqlConnection con =
        new SqlConnection(
            ConfigurationManager.
            ConnectionStrings["conexion"].ConnectionString);

    protected void Page_Load(object sender, EventArgs e)
    {
        cargarDatos();
    }
    void cargarDatos()
    {
        SqlCommand comando =
            new SqlCommand("sp_consultar_clientes", con);
        comando.CommandType = CommandType.StoredProcedure;
        con.Open();
        SqlDataAdapter adaptador = new SqlDataAdapter(comando);
        DataTable tabla = new DataTable();
        adaptador.Fill(tabla);
        gvClientes.DataSource = tabla;
        gvClientes.DataBind();
        con.Close();
    }



}//cierre de clase 